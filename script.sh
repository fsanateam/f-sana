#!/bin/bash

#run_time=5
mkdir -p sana_results_sqrt


#mkdir -p "$i"_run
for run_time in 3 5 10
do
	mkdir -p "$run_time"min

	for base_network in syeast0
	do
		for network in syeast05 syeast10 syeast15 syeast20 syeast25
		do
			
			for i in {1..5}
			do				
				result_path="$PWD"/"$base_network"_"$network"_"$i"
				mkdir -p "$result_path"
				
				for method in sana_improved 
				do
					./sana -method $method -g1 "$base_network" -g2 "$network" -t $run_time  
					mv -v "$PWD"/sana.out "$result_path"/"$method".out
					mv -v "$PWD"/sana.out.align "$result_path"/"$method".out.align
					for csv in "$PWD"/*"$network".csv
					do
						mv ${csv} "$result_path"
					done
				done 
				
				
			done
			
		done
		for result_file in "$PWD"/"$base_network"_*
		do
			mv $result_file "$run_time"min
		done
	done	
	
	#mv "$run_time"min "$i"_run
done

for run in "$PWD"/*_min
do		
	mv $run sana_results_sqrt
done

 
